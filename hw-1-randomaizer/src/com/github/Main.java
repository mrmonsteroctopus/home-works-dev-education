package com.github;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Main {

    public static void main(String[] args) {
        System.out.println("Enter   е  the range of numbers from " +
                "which you want to get a random number, "+ "\n" +
                "use the following rules for the program to work correctly "+ "\n" +
                "1.the minimum number cannot be less than 0 "+ "\n" +
                "2.the maximum number cannot be less than "+ "\n" +
                "the minimum number and greater than 500 "+ "\n" +
                "3.numbers can only be natural \n");
        Scanner numb = new Scanner(System.in);
        System.out.println("Enter minimum number \n");
        int minNumb = parseInt(numb.next());
        System.out.println("Enter maximum number \n");
        int maxNumb = parseInt(numb.next());
        int[] GeneratedNumb = new int[maxNumb+1 - minNumb];
        int ind = 0;
        for(int i = minNumb; i < maxNumb+1; i++){
            GeneratedNumb[ind] = i;
            ind++;
        }
        System.out.println(Arrays.toString(GeneratedNumb));


        label:
        while(true){
            System.out.println("Enter  the operation "+ "\n" +
                    "1.generate - (    if you want to generate a random number) "+ "\n" +
                    "2.exit - (if you want to stop the program) "+ "\n" +
                    "3.help - (if you want to get help with the program)\n");
            Scanner ask = new Scanner(System.in);
            String exit = ask.next();
            switch (exit) {
                case "help":
                    System.out.println("follow the rules written above");
                    break;
                case "exit":
                    System.out.println("stop");
                    break label;
                case "generate":
                    int random = (int) (minNumb + Math.random() * (maxNumb - minNumb));
                    for (int j = 0; j < GeneratedNumb.length; j++) {
                        if (GeneratedNumb[j] == random) {
                            GeneratedNumb[j] = 0;
                            System.out.println("Random number: " + Integer.toString(random));
                            break;
                        }
                    }
                    //random = (int) (minNumb + Math.random() * (maxNumb - minNumb));
                    //System.out.println("Random number: " + Integer.toString(random));
                    System.out.println(Arrays.toString(GeneratedNumb));
                    break;
            }
        }
    }
}
